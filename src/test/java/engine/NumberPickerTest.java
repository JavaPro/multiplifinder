package engine;

import com.sun.javaws.exceptions.InvalidArgumentException;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;


public class NumberPickerTest {

    @org.junit.jupiter.api.Test
    public void should_check_number_return__0() throws InvalidArgumentException {
        NumberPicker.setMinValueNumber(1);
        NumberPicker.setMaxValueNumber(10);
        NumberPicker.initNumbers(2, 3, 4);
        Assertions.assertEquals(0, NumberPicker.checkNumber(5));
    }

    @org.junit.jupiter.api.Test
    public void should_check_number_return__1() throws InvalidArgumentException {
        NumberPicker.setMinValueNumber(1);
        NumberPicker.setMaxValueNumber(10);
        NumberPicker.initNumbers(2, 3, 4);
        Assertions.assertEquals(1, NumberPicker.checkNumber(9));
    }

    @org.junit.jupiter.api.Test
    public void should_check_number_return__2() throws InvalidArgumentException {
        NumberPicker.setMinValueNumber(1);
        NumberPicker.setMaxValueNumber(10);
        NumberPicker.initNumbers(2, 3, 4);
        Assertions.assertEquals(2, NumberPicker.checkNumber(8));
    }

    @org.junit.jupiter.api.Test
    public void should_check_number_return__3() throws InvalidArgumentException {
        NumberPicker.setMinValueNumber(1);
        NumberPicker.setMaxValueNumber(10);
        NumberPicker.initNumbers(2, 3, 4);
        Assertions.assertEquals(3, NumberPicker.checkNumber(12));
    }

    @org.junit.jupiter.api.Test
    public void should_check_number_return__3_with_more_possibilities() throws InvalidArgumentException {
        NumberPicker.setMinValueNumber(1);
        NumberPicker.setMaxValueNumber(10);
        NumberPicker.initNumbers(2, 3, 4);
        Assertions.assertEquals(3, NumberPicker.checkNumber(48));
    }

    @org.junit.jupiter.api.Test
    public void should_throw_exception_when_initializing_with_wrong_parameters() throws InvalidArgumentException {
        NumberPicker.setMinValueNumber(5);
        NumberPicker.setMaxValueNumber(15);
        InvalidArgumentException thrown = assertThrows(
                InvalidArgumentException.class,
                () -> NumberPicker.initNumbers(1, 2, 3),
                "NumberPicker.initNumbers() should throw an error since parameters are below min value"
        );
    }
}
