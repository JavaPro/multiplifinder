package game;

import org.junit.jupiter.api.Assertions;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

public class MultipliFinderGameTest {


    @org.junit.jupiter.api.Test
    public void should_print_the_winning_message() {


        // Setup of the inputs/ outputs and values
        String userInput = "35" + System.lineSeparator() + "140";

        ByteArrayInputStream testUserInput = new ByteArrayInputStream(userInput.getBytes());
        Scanner myInput = new Scanner(testUserInput);
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        PrintStream myOutput = new PrintStream(outputStreamCaptor);

        // Real test

        MultipliFinderGame game = new MultipliFinderGame();
        game.init(myInput, myOutput);
        game.customGame(1, 10, 7, 5, 4);
        game.play();

        // Verifications

        Assertions.assertTrue(outputStreamCaptor.toString().contains("Congratulations, you won!"));
    }

    @org.junit.jupiter.api.Test
    public void should_not_print_the_winning_message() {


        // Setup of the inputs/ outputs and values
        String userInput = "35" + System.lineSeparator() + "80";

        ByteArrayInputStream testUserInput = new ByteArrayInputStream(userInput.getBytes());
        Scanner myInput = new Scanner(testUserInput);
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        PrintStream myOutput = new PrintStream(outputStreamCaptor);

        // Real test

        MultipliFinderGame game = new MultipliFinderGame();
        game.init(myInput, myOutput);
        game.customGame(1, 10, 7, 5, 4);
        game.play();

        // Verifications

        Assertions.assertFalse(outputStreamCaptor.toString().contains("Congratulations, you won!"));
    }

    @org.junit.jupiter.api.Test
    public void should_not_win_when_0_is_submitted() {


        // Setup of the inputs/ outputs and values
        String userInput = "35" + System.lineSeparator() + "0";

        ByteArrayInputStream testUserInput = new ByteArrayInputStream(userInput.getBytes());
        Scanner myInput = new Scanner(testUserInput);
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        PrintStream myOutput = new PrintStream(outputStreamCaptor);

        // Real test

        MultipliFinderGame game = new MultipliFinderGame();
        game.init(myInput, myOutput);
        game.customGame(1, 10, 7, 5, 4);
        game.play();

        // Verifications

        System.out.println(outputStreamCaptor.toString());
        Assertions.assertFalse(outputStreamCaptor.toString().contains("Congratulations, you won!"));
        Assertions.assertTrue(outputStreamCaptor.toString().contains("Score : 0"));
    }
}
