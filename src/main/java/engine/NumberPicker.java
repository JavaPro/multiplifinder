package engine;

import com.sun.javaws.exceptions.InvalidArgumentException;

import java.util.concurrent.ThreadLocalRandom;

public class NumberPicker {

    private static int MIN_VALUE_NUMBER = 1;
    private static int MAX_VALUE_NUMBER = 10;

    private static int[] numbers = {MIN_VALUE_NUMBER, MIN_VALUE_NUMBER, MIN_VALUE_NUMBER};

    public static void initNumbers() {
        int number1 = ThreadLocalRandom.current().nextInt(MIN_VALUE_NUMBER, MAX_VALUE_NUMBER + 1);
        int number2 = ThreadLocalRandom.current().nextInt(MIN_VALUE_NUMBER, MAX_VALUE_NUMBER + 1);
        int number3 = ThreadLocalRandom.current().nextInt(MIN_VALUE_NUMBER, MAX_VALUE_NUMBER + 1);

        try {
            initNumbers(number1, number2, number3);
        } catch (InvalidArgumentException e) {
            // This should never happen as we're creating the inputs ourselves
            e.printStackTrace();
        }
    }

    public static void initNumbers(int number1, int number2, int number3) throws InvalidArgumentException {
        if (number1 < MIN_VALUE_NUMBER || number1 > MAX_VALUE_NUMBER
            || number2 < MIN_VALUE_NUMBER || number2 > MAX_VALUE_NUMBER
            || number3 < MIN_VALUE_NUMBER || number3 > MAX_VALUE_NUMBER) {
            String[] errors = {String.format("The initializing numbers should be greater than %d and lesser than %d. Received values: %d, %d, %d." ,
                    MIN_VALUE_NUMBER, MAX_VALUE_NUMBER, number1, number2, number3)};
            throw new InvalidArgumentException(errors);
        }
        numbers[0] = number1;
        numbers[1] = number2;
        numbers[2] = number3;
    }

    /**
     * Checks how many numbers from our array are divided by the given number.
     * @param i - The number to test
     * @return The score obtained for this value
     */
    public static int checkNumber(int i) {
        int score = 0;
        for (int aNumber : numbers) {
            if (MathHelper.isAnAcceptableDivider(aNumber, i)) {
                score++;
            }
        }
        return score;
    }


    // GETTERS AND SETTERS

    public static int getMinValueNumber() {
        return MIN_VALUE_NUMBER;
    }

    public static void setMinValueNumber(int minValueNumber) {
        MIN_VALUE_NUMBER = minValueNumber;
    }

    public static int getMaxValueNumber() {
        return MAX_VALUE_NUMBER;
    }

    public static void setMaxValueNumber(int maxValueNumber) {
        MAX_VALUE_NUMBER = maxValueNumber;
    }

}
