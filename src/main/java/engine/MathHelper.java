package engine;

public class MathHelper {


    public static boolean isAnAcceptableDivider(int numberToTest, int numberBeingDivided) {
        return numberBeingDivided != 0 && numberBeingDivided % numberToTest == 0;
    }
}
