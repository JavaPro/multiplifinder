package game;

import com.sun.javaws.exceptions.InvalidArgumentException;
import engine.NumberPicker;

import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MultipliFinderGame {

    private Scanner input;
    private PrintStream output;

    private final String START_GAME_MESSAGE = "Welcome to the Number Finder game! I have generated 3 numbers. " +
            "Your goal is to enter a number that is a multiple of the 3 secret numbers generated.\n" +
            "Each time you input a number I(ll show you a result between 0 and 3\n" +
            "The result is determined by the amount of secret numbers that are a factor of the submitted number\n" +
            "The game ends when you find a multiple of all 3 secret numbers.\n" +
            "0 is not accepted as a solution.";
    private final String PROMPT_INPUT_NUMBER = "Please enter a number : ";
    private final String GAME_FINISHED_MESSAGE = "Game finished! Thanks for playing with me!";
    private String WINNING_MESSAGE = "Congratulations, you won!";

    public void init(Scanner aInput, PrintStream aOutput) {
        input = aInput;
        output = aOutput;
        defaultGame();
    }

    /**
     * Creates a game with min value of 1 and max value of 10. Numbers are chosen randomly.
     */
    public void defaultGame() {
        NumberPicker.setMinValueNumber(1);
        NumberPicker.setMaxValueNumber(10);
        NumberPicker.initNumbers();
    }

    /**
     * Creates a game with specified lower and upper bound, and chosen numbers.
     */
    public void customGame(int minValue, int maxValue, int number1, int number2, int number3) {
        NumberPicker.setMinValueNumber(minValue);
        NumberPicker.setMaxValueNumber(maxValue);
        try {
            NumberPicker.initNumbers(number1, number2, number3);
        } catch (InvalidArgumentException e) {
            System.err.println("Bad arguments for the custom game initialization. Defaulting to a random game initialization.");
            System.err.println("Error was : " + e.getRealMessage());
            NumberPicker.initNumbers();
        }
    }

    public void play() {
        output.println(START_GAME_MESSAGE);
        while (true) {
            output.println(PROMPT_INPUT_NUMBER);
            try {
                int submittedNumber = input.nextInt();
                int score = NumberPicker.checkNumber(submittedNumber);
                if (score == 3) {
                    win();
                    break;
                }
                outputScore(score);
            } catch (InputMismatchException e) {
                output.println("This is not a readable number.");
                input.nextLine();
            } catch (NoSuchElementException | IllegalStateException ioException) {
                output.println("An error occurred while trying to read the input. Please restart your game.");
                break;
            }
        }
        output.println(GAME_FINISHED_MESSAGE);
    }

    private void outputScore(int score) {
        output.println("Score : " + score);
    }

    private void win() {
        output.println(WINNING_MESSAGE);
    }
}
