import game.MultipliFinderGame;

import java.util.Scanner;

/**
 * Entry point for project Multiplifinder.
 * By JavaPro
 */


public class Launcher {

    public static void main(String[] args) {
        MultipliFinderGame game = new MultipliFinderGame();

        // While true get user input and play
        Scanner input = new Scanner(System.in);
        game.init(input, System.out);
        game.play();
    }
}
