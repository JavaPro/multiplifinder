# multiplifinder

Coding test as interview material.
The game generates 3 secret numbers between 1 and 10.
The goal of the player is to enter a number that is a multiple of the 3 secret numbers generated.
Each time the player inputs a number the game shows the player a result between 0 and 3
The result is determined by the amount of secret numbers that are a factor of the submitted number
The game ends when the player finds a multiple of all 3 secret numbers

# Personal improvements

- Removed 0 from the solutions
- Added the possibility to choose the range of numbers generated (4 to 50 instead of the default 1 to 10 for example).

# Suggested improvements

- Since the range of possible numbers is known, there always exists evident solutions that will work whatever the numbers chosen. As an example, for a range 1-10, the number 5040 will always win. To fix that, trying to aim for the lowest multiplier would be better.
- Adding a limit of tries and/or a time limit would make the game more challenging.
- Adding a multiplier version would make this a funny minigame to play with friends.


-------------------

Time spent on the project: About 3 hours (2h30, break, 30 minutes).